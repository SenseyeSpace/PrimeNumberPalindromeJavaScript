# Palindrome made from the product of two 5-digit prime numbers

##### Result:
```html
Palindrome: 999949999
Prime number left: 33211
Prime number right: 30109
Duration: 8839 ms
```

##### Resources:
* [Sieve of Eratosthenes](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes)
* [How to initialize a boolean array in JavaScript](https://stackoverflow.com/questions/27040430/how-to-initialize-a-boolean-array-in-javascript)
```javascript
console.log(new Array(5).fill(true));
```

##### Optimize:
* Ознака ділення на 11 — Число ділиться на 11, якщо сума цифр на парних місцях мінус сума на непарних ділиться на 11
* [10-значні паліндроми всі діляться на 11](https://dou.ua/forums/topic/25122/#1424866)
* [GPU](https://dou.ua/forums/topic/25122/#1424914)

##### Languages:
* [Golang](https://gitlab.com/Senseye/PrimeNumberPalindromeGolang)
* [Python](https://gitlab.com/Senseye/PrimeNumberPalindromePython)
* [C](https://gitlab.com/Senseye/PrimeNumberPalindromeC)
* [Rust](https://gitlab.com/Senseye/PrimeNumberPalindromeRust)